///////////////////////////////////////////////// SETTER
function setFacebookCount(res) {
	if(typeof(res.error_code === 'undefined') && res.length) {
		$('.fb_count').html(number_format(res[0].fan_count, 0, '', ' '));
	}
}
function setTwitterCount(res) {
	$('.tw_count').html(number_format(res, 0, '', ' '));
}
function setVkontakteCount(res) {
	if(typeof(res['response']) === 'undefined') return;
	$('.vk_count').html(number_format(res['response']['count'], 0, '', ' '));
}
///////////////////////////////////////////////// GETTER
function getFacebookCount(url) {
	injectScript('http://api.facebook.com/method/fql.query?format=json&query=select+fan_count+from+page+where+page_id%3D' + url + '&callback=setFacebookCount');
}
function getTwitterCount(url) {
	$.getJSON(	"https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20html%20where%20url%3D'http%3A%2F%2Ftwitter.com%2F" + url + "'%20and%20xpath%3D'%2F%2Finput%5B%40id%3D%22init-data%22%5D'&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys",
		function (data) {
			setTwitterCount(data.query.results.input.value.match(/followers_count":(\d+)/)[1]);
		}
	);
}
function getVkontakteCount(url) {
	injectScript('https://api.vk.com/method/groups.getMembers?group_id=' + url + '&callback=setVkontakteCount');
}
function getGooglePlusCount(profileid) {
	var apikey = 'AIzaSyBPGJ7_hfDBD8su2aXc05fjEDi1h8bWtdA';
	var url = 'https://www.googleapis.com/plus/v1/people/' + profileid + '?key=' + apikey;

		$.ajax({
		type: "GET",
		dataType: "json",
		url: url,
		success: function (data) {
			$(".gp_count").html(number_format(data.circledByCount, 0, '', ' '));
		}
	});
}
function getYoutubeCount(channelid) {
	var apikey = 'AIzaSyBPGJ7_hfDBD8su2aXc05fjEDi1h8bWtdA';
	var url = 'https://www.googleapis.com/youtube/v3/channels?part=statistics&forUsername=' + channelid + '&key=' + apikey;

	$.ajax({
		type: "GET",
		dataType: "json",
		url: url,
		success: function (data) {
			$(".yt_count").html(number_format(data.items[0].statistics.subscriberCount, 0, '', ' '));
		}
	});
	
}
function injectScript(url) {
	var script = document.createElement('script');
	script.async = true;
	script.src = url;
	document.body.appendChild(script);
}
function number_format(number, decimals, dec_point, thousands_sep) {
	var i, j, kw, kd, km;

	if( isNaN(decimals = Math.abs(decimals)) ){
		decimals = 2;
	}
	if( dec_point == undefined ){
		dec_point = ",";
	}
	if( thousands_sep == undefined ){
		thousands_sep = ".";
	}

	i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

	if( (j = i.length) > 3 ){
		j = j % 3;
	} else{
		j = 0;
	}

	km = (j ? i.substr(0, j) + thousands_sep : "");
	kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
	kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");

	return km + kw + kd;
}


