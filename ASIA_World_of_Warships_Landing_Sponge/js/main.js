$(function () {

	//email validation rules
	function validateEmail(email) {
		var re = new RegExp('^([A-Za-z0-9_-]+.)*[A-Za-z0-9_-]+@[a-z0-9_-]+(.[a-z0-9_-]+)*.[a-z]{2,6}$');
		return re.test(email.val());
	}

	//password validation rules
	function validatePass(pass) {
		var re = /^[a-z0-9_]+$/;
		return re.test(pass.val());
	}

	function validatePassLength(item) {
		return item.val().length >= 6;
	}

	function equalto(i1, i2) {
		return i1.val() === i2.val()
	}

	//nickname validation rules
	function validateName(pass) {
		var re = /^[A-Za-z0-9_]+$/;
		return re.test(pass.val());
	}

	function validateNameLength(item) {
		return item.val().length >= 3;
	}

	//cap validation rules
	function validateCap(pass) {
		var re = /^[0-9]+$/;
		return re.test(pass.val());
	}

	//promo validation rules
	function validatePromo(pass) {
		var re = /^[A-Z0-9-]+$/;
		return re.test(pass.val());
	}

	//check if filled
	function isfilled(item) {
		return item.val().length > 0
	}

	//set field status after validation
	function setStatus(item, status) {
		var parent = item.parents('.regform-item');
		parent.removeClass('_ok').removeClass('_err');
		parent.addClass(status);
	}

	//set field message after validation
	function setMessage(item, msg) {
		var parent = item.parents('.regform-item');
		parent.find('.regform-item-hint').html(msg);
	}


	//validation logic
	function validateItem(item) {
		//console.log(item.data('type'));
		var result = false,
			msg = '',
			type = item.data('type');

		if (type === 'login') {
			if (isfilled(item)) {
				if (!validateEmail(item)) {
					result = false;
					msg = formMessages.login.invalid;
				} else {
					result = true;
					msg = formMessages.login.def;
				}
			} else {
				result = false;
				msg = formMessages.login.required;
			}
		}
		if (type === 'name') {
			if (isfilled(item)) {
				if (!validateName(item)) {
					result = false;
					msg = formMessages.name.invalid;
				} else if (!validateNameLength(item)) {
					result = false;
					msg = formMessages.name.min_length;
				} else {
					result = true;
					msg = formMessages.name.def;
				}
			} else {
				result = false;
				msg = formMessages.name.required;
			}
		}
		if (type === 'password') {
			if (isfilled(item)) {
				if (!validatePassLength(item)) {
					result = false;
					msg = formMessages.password.min_length;
				} else {
					if (validatePass(item)) {
						if (isfilled($('.required-data[name=repeat_password]'))) {
							if (!equalto(item, $('.required-data[name=repeat_password]'))) {
								result = false;
								msg = formMessages.password.notequal;
							} else {
								result = true;
								msg = formMessages.password.def;
							}
						} else {
							result = false;
							msg = formMessages.password.reprequired;
						}
					} else {
						result = false;
						msg = formMessages.password.invalid;

					}
				}
			} else {
				result = false;
				msg = formMessages.password.required;
			}
		}
		if (item.attr('name') === 'repeat_password') {
			if (isfilled($('.required-data[name=password]'))) {
				if (isfilled(item)) {
					if (!equalto(item, $('.required-data[name=password]'))) {
						result = false;
						msg = formMessages.password.notequal;
					} else {
						var obj = validateItem($('.required-data[name=password]'));
						if (obj.result) {
							result = true;
							msg = formMessages.password.def;
						}
					}
				} else {
					result = false;
					msg = formMessages.password.reprequired;
				}
			} else {
				result = false;
				msg = formMessages.password.required;
			}
		}
		if (type === 'cap') {
			if (isfilled(item)) {
				if (validateCap(item)) {
					result = true;
					msg = formMessages.cap.def;
				} else {
					result = false;
					msg = formMessages.cap.invalid;
				}
			} else {
				result = false;
				msg = formMessages.cap.required;
			}
		}
		if (type === 'promo') {
			if (isfilled(item)) {
				if (validatePromo(item)) {
					result = true;
					msg = formMessages.promo.def;
				} else {
					result = false;
					msg = formMessages.promo.invalid;
				}
			} else {
				result = true;
				msg = formMessages.promo.def;
			}
		}
		if (type === 'agree') {
			if (item.prop("checked")) {
				result = true;
				msg = formMessages.agree.def;
			} else {
				result = false;
				msg = formMessages.agree.required;
			}
		}
		return {
			result: result,
			msg: msg
		}
	}

	var datainput = $('.required-data');

	datainput.on('blur', function () {
		var type = $(this).data('type');
		if (type != 'cap' && type != 'promo') {
			validateinput($(this));
		}
	});

	(function () {
		var timeout;
		datainput.on('input propertychange', function () {
			var type = $(this).data('type');
			var that = $(this);
			clearTimeout(timeout);
			timeout = setTimeout(function (type) {
				if (type != 'cap') {
					validateinput(that);
				}
			}, 1000);
		});
	}());

	$('.regform-item-inp').on('input propertychange', function () {
		var type = $(this).data('type');
		if (type === 'cap' || type === 'promo') {
			validateinput($(this));
		}
	});

	function validateinput(item) {
		var inptype = item.data('type'),
			obj, msg;
		if (item.val().length > 0) {
			obj = validateItem(item);
			if (obj.result) {
				msg = formMessages[item.data('type')].def;
				setStatus(item, '_ok');
				setMessage(item, msg)
			} else {
				setStatus(item, '_err');
				setMessage(item, obj.msg)
			}
		} else {
			if (item.attr('name') == 'repeat_password' && $('.regform-item-inp[name="password"]').val() != '') {
				setStatus(item, '_err');
				setMessage(item, formMessages.password.notequal)
			} else {
				setMessage(item, formMessages[item.data('type')].def);
				item.parents('.regform-item').removeClass('_ok');
				item.parents('.regform-item').removeClass('_err');
			}
		}
	}


	datainput.on('focus', function () {
		var type = $(this).data('type');
		if (type != 'cap' && !$(this).parents('.regform-item').hasClass('_err')) {
			$(this).parents('.regform-item').removeClass('_err').removeClass('_ok');
			var msg = formMessages[$(this).data('type')].def;
			setMessage($(this), msg);
		}
	});

	$('.regform-item-inp[name="captcha"]').focus(function () {
		$(this).removeClass('_err');
	});

	function checkForm(e) {
		$('#regForm').submit();
	}

	function submitForm(e) {
		var validarr = [],
			valid;
		$(this).find('.required-data').each(function () {
			var obj = validateItem($(this)),
				result = obj.result;
			if (result) {
				var msg = formMessages[$(this).data('type')].def;
				$(this).parents('.regform-item').removeClass('_ok');
				$(this).parents('.regform-item').removeClass('_err');
				setStatus($(this), '_ok');
				setMessage($(this), msg);
			} else {
				$(this).parents('.regform-item').removeClass('_ok');
				$(this).parents('.regform-item').removeClass('_err');
				setStatus($(this), '_err');
				setMessage($(this), obj.msg);
			}
			validarr.push(result);
		});
		valid = $.inArray(false, validarr);
		if (valid === -1) {
		} else {
			var firstError = $('.regform-item._err').eq(0);
			firstError.find('input').eq(0).focus();
			$('html, body').animate({
				scrollTop: firstError.offset().top - $(window).height() / 5
			});
			setStatus(firstError.find('.required-data').eq(0), '_err');
			e.preventDefault();
			return false;
		}

		var sid = '';
		var locationUrl = window.location.href;
		var urlParams = locationUrl.substring(locationUrl.indexOf('?') + 1);
		if (urlParams != '' && urlParams.indexOf('sid') != -1) {
			var sid = urlParams.substring(urlParams.indexOf('sid=') + 4);
			if (sid.indexOf('&') != -1) {
				sid = sid.substring(0, sid.indexOf('&'));
			}
		}
		$(this).append($('<input>', {
			'name': 'sid',
			'value': sid,
			'type': 'hidden'
		}));

		var queryString = $(this).serialize();

		$('.regform-send').unbind('click', checkForm);
		$('.regform-send-stripes').addClass('animate-lines');
		//the data could now be submitted using $.post, $.ajax (POST request)
		$.ajax({
			type: 'POST',
			url: RegApiURL,
			data: queryString,
			dataType: 'json',
			crossDomain: true,
			success: function (data) {
				if (data.status == 'ok') {
					reload_captcha();
					$('.regform-item-inp').val('');
					$('.regform-item').removeClass('_err').removeClass('_ok');

					if (html.hasClass('desktop')) {
						$('.regform').css('height', $('.regform').outerHeight());
					} else if (html.hasClass('mobile')) {
						$('article .fourth-block').css({'height': '500px', 'overflow': 'hidden'});
						$('article .fourth-block .form').css({'height': '100%'});
					} else if (html.hasClass('tablet')) {
						$('article .fourth-block').css({'height': '550px', 'overflow': 'hidden'});
						$('article .fourth-block .form').css({'height': '100%'});
					}
					$('.regform-message', $('.regform')).removeClass('js-hidden');
					$('.regform-data', $('.regform')).addClass('js-hidden');
				} else if (data.status == 'error') {
					reload_captcha();
					$('.regform-item-inp[name="captcha"]').val('');
					$('input[type="password"]').val('');
					$('input[type="password"]').parents('.regform-item').removeClass('_err').removeClass('_ok');
					if (data.error == 'conflicted_login') {
						setStatus($("[name='login']"), '_err');
						setMessage($("[name='login']"), formMessages.login.forbidden)
					}
					if (data.error == 'conflicted_nickname') {
						setStatus($("[name='name']"), '_err');
						setMessage($("[name='name']"), formMessages.name.forbidden)
					}
					if (data.error == 'invalid_form') {
						//alert(formMessages.invalid_form);
					}
					if (data.error == 'internal_error') {
						//alert(formMessages.internal_error);
					}
					for (var k in data.errors_codes) {
						if (data.errors_codes.hasOwnProperty(k)) {
							for (var index = 0; index < data.errors_codes[k].length; ++index) {
								setMessage($("[name='" + k + "']"), formMessages[k][data.errors_codes[k][index]]);
								setStatus($("[name='" + k + "']"), '_err');
								if (k == "cap") {
									$('.regform-item-inp[name="captcha"]').addClass('_err');
									//alert(formMessages[k][data.errors_codes[k][index]]);
								}
							}
						}
					}
					/*var firstError = $('.regform-item._err').eq(0);
					 firstError.find('input').eq(0).focus();
					 $('html, body').animate({
					 scrollTop: firstError.offset().top - $(window).height() / 5
					 });*/
				}
				$('.regform-send').click(checkForm);
				$('.regform-send-stripes').removeClass('animate-lines');
			},
			error: function (data) {
				//do anything if needed
			}
		});

		//return false to prevent normal browser submit and page navigation
		return false;
	}

	$('#regForm').submit(submitForm);
	$('.regform-send').click(checkForm);

	var firstInpNode = $('.regform-item-inp').eq(0);
	firstInpNode.blur();
	// firstInpNode.focus().focus();

	$('.regform-item-reloadcap').on('click', reload_captcha);

	function callCaptchaAjax() {
		//set form jQuery object
		$RegForm = $('#regForm');
		//save realm value to hidden input
		var $realm = $('<input name="realm" type="hidden" value="' + Realm + '"/>');
		$RegForm.append($realm);
		//create captcha request (GET)
		$.ajax({
			type: 'GET',
			url: RegApiURL,
			data: "realm=" + Realm,
			crossDomain: true,
			success: function (sessionId) {
				SessionId = sessionId;
				//load captcha image by creating GET request with sessionId and realm parameters
				$('#captcha_img').remove();
				var $captcha = $('<img class="regform-item-cap-pic" id="captcha_img">');
				$captcha.attr('id', 'captcha_img');
				$captcha.attr('class', "captcha");
				$captcha.prependTo('.regform-item-cap');
				$captcha.attr('src', RegApiURL + "?session_id=" + SessionId + "&realm=" + Realm);

				//save session id to hidden input
				$('#session_id').remove();
				var $sessionId = $('<input name="session_id" id="session_id" type="hidden" value="' + SessionId + '"/>');
				$RegForm.append($sessionId);

			}
		});
	}

	callCaptchaAjax();

	function reload_captcha() {
		callCaptchaAjax();
	}

	function cprDate() {
		var node = $('.footer-cpr-date'),
			date = parseInt(node.text()),
			curYear = new Date().getFullYear();

		if (date != curYear) {
			node.text(date + '‒' + curYear)
		} else if (date === curYear) {
			node.text(date)
		}

	}

	cprDate();

	function removehidden() {
		var nodes = $("[hide='1']");
		nodes.remove();
	}

	removehidden();
});
