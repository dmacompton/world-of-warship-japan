var $ = jQuery;
$(document).ready(function () {
	$('.container-loader').hide();

	$(".youtube").colorbox({iframe: true, width: "80%", height: "80%"});
	var hamburger = $('#hamburger-icon');
	hamburger.click(function () {
		hamburger.toggleClass('active');
		$('#hamburger-menu').toggleClass('hamburger-menu-view');
		$('body').toggleClass('fixedHeight');
		return false;
	});

	$(".red-registration-button").click(function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $("#regForm").offset().top
		}, 1000);
	});

	var windowWidth = $(window).width();
	if (windowWidth > 1036 && $('html').hasClass('desktop')) {
		var girls = [
			{
				selector: '.main-logos .first-girl-right',
				before: {
					left: '',
					right: '-664px',
					top: '300px'
				},
				after: {
					left: '',
					right: '-47px',
					top: '-20px'
				}
			},
			{
				selector: '.second-block .second-girl-left',
				before: {
					left: '-827px'
				},
				after: {
					left: '-8px'
				}
			},
			{
				selector: '.third-block .third-girl-image',
				before: {
					left: '100%'
				},
				after: {
					left: '74%'
				}
			},
			{
				selector: '.fourth-block .fourth-girl-image',
				before: {
					left: '-965px'
				},
				after: {
					left: '-178px'
				}
			}
		];

		var blocks = [
			'header',
			'.main-logos',
			'.second-block .right-image',
			'.second-block .left-side',
			'.third-block .third-ship-image',
			'.third-block .title-third-block',
			'.fourth-block .form',
			'.blue-watch-button, .red-registration-button',
			'article .second-block .wow-logo',
			'article .third-block .bluesteel-logo'
		];

		for (var block in blocks) {
			if (blocks.hasOwnProperty(block)) {
				var offset = '60%';
				$(blocks[block]).css({opacity: 0});
				new Waypoint({
					element: $(blocks[block]),
					offset: offset,
					handler: function () {
						var duration = 800;
						if (this.element.selector == '.main-logos') {
							$('.blue-watch-button').animate({
								opacity: 1
							}, {duration: 2000});
							$('.red-registration-button').animate({
								opacity: 1
							}, {duration: 2200});
						}
						if (this.element.selector == '.second-block .right-image' || this.element.selector == 'article .third-block .bluesteel-logo' || this.element.selector == 'article .second-block .wow-logo') {
							duration = 1200;
						}
						$(this.element).animate({
							opacity: 1
						}, {duration: duration});
					}
				});
			}
		}

		for (var girl in girls) {
			if (girls.hasOwnProperty(girl)) {
				if (girls[girl].selector == '.main-logos .first-girl-right') {
					$('.main-logos .first-girl-right').css(
						{
							right: girls[girl].before.right,
							top: girls[girl].before.top,
							opacity: 0
						}
					);
				} else {
					$(girls[girl].selector).css(
						{
							left: girls[girl].before.left,
							opacity: 0
						}
					);
				}

				new Waypoint({
					element: $(girls[girl].selector),
					after: girls[girl].after,
					offset: '60%',
					handler: function () {
						if (this.element.selector == '.main-logos .first-girl-right') {
							$(this.element.selector).animate({
								right: this.options.after.right,
								top: this.options.after.top,
								opacity: 1
							}, {duration: 1500});
						} else {
							$(this.element.selector).animate({
								left: this.options.after.left,
								opacity: 1
							}, {duration: 1000});
						}
					}
				});
			}
		}
	}

	$('.regform-item-promocode').on('click', function () {
		if (!$(this).parents('.regform-item').hasClass('_err')) {
			$(this).next('.regform-item-promocode-wrp').stop().slideToggle();
		}
	});

	if ($('html').hasClass('desktop')) {
		$('.social-block a').bind('mouseenter mouseleave click', function (e) {
			if (e.type === 'mouseenter') {
				$(this).removeClass('release').addClass('push');
			}
			if (e.type === 'mouseleave') {
				$(this).removeClass('push').addClass('release').delay(1100).queue(function (next) {
					$(this).removeClass("release");
					next();
				});
			}
			if (e.type === 'click') {
				$(this).addClass('push').delay(300).queue(function (next) {
					$(this).removeClass("push");
					next();
				});
			}
		});
	}
});


/////////////////////////////////// show or hide hamburger menu /////////////////////////////////////////////////

if (!hideHamburger) {
	$('.hamburger').hide();
}

/////////////////////////////////// get social counter /////////////////////////////////////////////////

if (urlVkontakte.id.length && urlVkontakte.visibility) {
	getVkontakteCount(urlVkontakte.id);
}
if (urlFacebook.id.length && urlFacebook.visibility) {
	getFacebookCount(urlFacebook.id);
}
if (urlTwitter.id.length && urlTwitter.visibility) {
	getTwitterCount(urlTwitter.id);
}
if (urlYoutube.id.length && urlYoutube.visibility) {
	getYoutubeCount(urlYoutube.id);
}
if (urlGooglePlus.id.length && urlGooglePlus.visibility) {
	getGooglePlusCount(urlGooglePlus.id);
}

/////////////////////////////////// change footer social width /////////////////////////////////////////////////

if (count_blocks == 0){
	$('.social-block').children('a:last-child').hide();
} else if (count_blocks == 2) {
	$('.social-block').addClass('social-block-3');
} else if (count_blocks == 3) {
	$('.social-block').addClass('social-block-2');
} else if (count_blocks == 4) {
	$('.social-block').addClass('social-block-1');
}

/////////////////////////////////// change footer social width /////////////////////////////////////////////////

var html = $('html');
if (html.hasClass('tablet')) {
	$('body').attr('style', 'background-image: url(' + bgs.tablet + ')');
	$('meta[name=viewport]').remove();
	addImage('tablet', images);
} else if (html.hasClass('mobile')) {
	$('body').attr('style', 'background-image: url(' + bgs.mobile + ')');
	$('meta[name=viewport]').remove();
	addImage('mobile', images);
} else if (html.hasClass('desktop')) {
	$('body').attr('style', 'background-image: url(' + bgs.desktop + ')');
	addImage('desktop', images);
} else {
	$('body').attr('style', 'background-image: url(' + bgs.desktop + ')');
}

function addImage(type, images) {
	for (var i = 0; i < images.length; i++) {
		$(images[i]['selector']).attr('src', images[i][type]);
	}
}

$('#currYear').text(new Date().getFullYear());

function PopUpShow() {
	$("#popup1").show();
}
function PopUpHide() {
	$("#popup1").hide();
}
$(document).ready(function () {

	$(".youtube").colorbox({iframe: true, innerWidth: 640, innerHeight: 390});

	$(document).on("click", '.fb-share', function () {
		var url = $(location).attr('href');
		Share.facebook(url)
	});
	$(document).on("click", '.vk-share', function () {
		var mainUrl = $(location).attr('hostname');
		var url = $(location).attr('href');
		var title = $('meta[property="og:title"]').attr('content');
		var image = mainUrl + $('meta[property="og:image"]').attr('content');
		var text = $('meta[property="og:description"]').attr('content');
		Share.vkontakte(url, title, image, text)
	});
	$(document).on("click", '.tw-share', function () {
		var url = $(location).attr('href');
		var title = $('meta[property="og:title"]').attr('content') + ' ' + $('meta[property="og:description"]').attr('content');
		Share.twitter(url, title)
	});
	$(document).on("click", '.gp-share', function () {
		var url = $(location).attr('href');
		Share.google(url);
	});
});

Share = {
	vkontakte: function (purl, ptitle, pimg, text) {
		var url = 'http://vkontakte.ru/share.php?';
		url += 'url=' + encodeURIComponent(purl);
		url += '&title=' + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&image=' + encodeURIComponent(pimg);
		url += '&noparse=true';
		Share.popup(url);
	},
	facebook: function (purl) {
		var url = "http://www.facebook.com/sharer.php?u=" + purl;
		Share.popup(url);
	},
	twitter: function (purl, ptitle) {
		var url = 'http://twitter.com/share?';
		url += 'text=' + encodeURIComponent(ptitle);
		url += '&url=' + encodeURIComponent(purl);
		url += '&counturl=' + encodeURIComponent(purl);
		Share.popup(url);
	},
	google: function (url) {
		var url = 'https://plusone.google.com/_/+1/confirm?hl=en&url=' + encodeURIComponent(url);
		Share.popup(url);
	},
	popup: function (url) {
		window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
	}
};

// IE detect
var class_name = '';
if (getInternetExplorerVersion() != -1) {
	class_name = 'ie';
} else {
	class_name = 'not_ie';
}
var classes = document.getElementsByTagName("html")[0].getAttribute("class");
document.getElementsByTagName("html")[0].setAttribute("class", classes + " " + class_name);


function getInternetExplorerVersion() {
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}