"use strict"

var gulp = require('gulp'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	autoprefixer = require('gulp-autoprefixer'),
	minifyCss = require('gulp-minify-css'),
	connect = require('gulp-connect'),
	sass = require('gulp-sass'),
	uglify = require('gulp-uglify'),
	htmlmin = require('gulp-htmlmin'),
	livereload = require('gulp-livereload');

gulp.task('connect', function() {
	connect.server({
		root: 'ASIA_World_of_Warships_Landing_Sponge/',
		livereload: true
	});
});

gulp.task('css', function() {
	gulp.src('ASIA_World_of_Warships_Landing_Sponge/css/scss/style.scss')
		.pipe(sass())
		.pipe(autoprefixer('last 15 versions'))
		.pipe(concat('main.css'))
		.pipe(minifyCss())
		.pipe(rename('style.min.css'))
		.pipe(gulp.dest('ASIA_World_of_Warships_Landing_Sponge/css/'))
		.pipe(connect.reload());
});

gulp.task('js', function() {
	gulp.src([
		'ASIA_World_of_Warships_Landing_Sponge/js/jquery-1.10.2.min.js',
		'ASIA_World_of_Warships_Landing_Sponge/js/modernizr.js',
		'ASIA_World_of_Warships_Landing_Sponge/js/device.js',
		'ASIA_World_of_Warships_Landing_Sponge/js/jquery.waypoints.js',
		'ASIA_World_of_Warships_Landing_Sponge/js/jquery.colorbox.js',
		'ASIA_World_of_Warships_Landing_Sponge/js/script.js',
		'ASIA_World_of_Warships_Landing_Sponge/js/social.js',
		'ASIA_World_of_Warships_Landing_Sponge/js/main.js'
	])
		.pipe(uglify())
		.pipe(concat('script.min.js'))
		.pipe(gulp.dest('ASIA_World_of_Warships_Landing_Sponge/'))
});

gulp.task('html', function() {
	gulp.src('ASIA_World_of_Warships_Landing_Sponge/ASIA_World_of_Warships_Landing_Sponge.html')
		//.pipe(htmlmin({collapseWhitespace: true}))
		//.pipe(rename('index.min.html'))
		.pipe(gulp.dest('ASIA_World_of_Warships_Landing_Sponge/'))
		.pipe(connect.reload());
});

gulp.task('watch', function() {
	gulp.watch('ASIA_World_of_Warships_Landing_Sponge/css/scss/includes/*.scss', ['css']);
	gulp.watch('ASIA_World_of_Warships_Landing_Sponge/css/scss/*.scss', ['css']);
	gulp.watch('ASIA_World_of_Warships_Landing_Sponge/ASIA_World_of_Warships_Landing_Sponge.html', ['html']);
	gulp.watch('ASIA_World_of_Warships_Landing_Sponge/js/*.js', ['js']);
	// gulp.watch('ASIA_World_of_Warships_Landing_Sponge/dist/*.html', ['webserver']);
});


// default
gulp.task('default', [
	'connect',
	'html',
	'css',
	'js',
	'watch'
]);